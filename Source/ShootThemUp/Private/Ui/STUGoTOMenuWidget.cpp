// ShootThemUpGame Test Project (IUSOV)


#include "Ui/STUGoTOMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "STUGameInstance.h"

void USTUGoTOMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GoToMenuButton)
    {
        GoToMenuButton->OnClicked.AddDynamic(this, &USTUGoTOMenuWidget::OnGoToMenuGame);
    }
}

void USTUGoTOMenuWidget::OnGoToMenuGame() {
    if (!GetWorld()) return;

    const auto STUGameInstance = GetWorld()->GetGameInstance<USTUGameInstance>();
    if (!STUGameInstance) return;

    if (STUGameInstance->GetMenuLevelName().IsNone())
    {
        return;
    }
    UGameplayStatics::OpenLevel(this, STUGameInstance->GetMenuLevelName());
}
