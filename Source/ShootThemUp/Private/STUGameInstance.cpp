// ShootThemUpGame Test Project (IUSOV)


#include "STUGameInstance.h"
#include "Sound/STUSoundLibrary.h"

void USTUGameInstance::ToggleVolume() 
{
    USTUSoundLibrary::ToggleSoundClassVolume(MasterSoundClass);
}
